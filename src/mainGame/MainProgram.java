package mainGame;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javafx.application.Application;
import javafx.scene.Scene;

import javafx.stage.Stage;

public class MainProgram extends Application {

	public static void main(String[] args) {

		launch(args);

	}

	public static Stage primaryStage;
	static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	public static int width = gd.getDisplayMode().getWidth();
	public static int height = gd.getDisplayMode().getHeight();

	@Override
	public void start(Stage Stage) {
		primaryStage = Stage;
		primaryStage.setTitle("The Escape");

		primaryStage.setScene(new Scene(scenes.OpeningScreen.scene1(), width, height));
		scenes.OpeningScreen.scene2().getStylesheets()
				.add(MainProgram.class.getResource("/cssFiles/TitleScreen.css").toExternalForm());
		primaryStage.show();
	}

	public static int[][] createRandomMatrix(String[] monsterNames) {// A method that creates a matrix with random values in the second row
		int monsterWeakness[][] = new int[monsterNames.length][2];
		for (int i = 0; i < monsterWeakness.length; i++) {
			for (int j = 0; j < 2; j++) {
				monsterWeakness[i][j] = (int) (Math.random() * 3);
			}
		}
		return monsterWeakness;

	} 

}