package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import mainGame.Player;

public class CharacterCreation {

	public static Scene sceneCC() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		Text scenetitle = new Text("Choose your stats:");
		grid.add(scenetitle, 0, 0, 2, 1);
		scenetitle.setId("welcome-text");

		final Text actiontarget = new Text();
		actiontarget.setId("actiontarget");
		grid.add(actiontarget, 3, 16);

		Label points = new Label("Available Skill points:");
		grid.add(points, 0, 5);

		Label potions = new Label("You have: " + Player.potions + " potions");
		potions.setId("availablep");
		grid.add(potions, 0, 6);

		Label availablepoints = new Label(); // Skillpoints
		availablepoints.setText("" + Player.skillpoints);
		grid.add(availablepoints, 1, 5);

		Label health = new Label("Health " + Player.healthHero);
		grid.add(health, 0, 9);

		Button btn1 = new Button(); // Button for incerasing health
		btn1.setText(" + ");
		grid.add(btn1, 1, 9);
		btn1.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing health +
															// button

			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 0) {
					actiontarget.setText("Out of skillpoints");
					Player.skillpoints += 1;
					Player.healthHero -= 1;
				}
				Player.skillpoints -= 1;
				health.setText("Health: " + (Player.healthHero + 1));
				availablepoints.setText("" + Player.skillpoints);
				Player.healthHero += 1;
			}
		}); 

		Label defence = new Label("Defence: " + Player.defenceHero);
		grid.add(defence, 0, 10);

		Button btn2 = new Button(); // Button for increasing defence
		btn2.setText(" + ");
		grid.add(btn2, 1, 10);
		btn2.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing defence+
															// button

			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 0) {
					actiontarget.setText("Out of skillpoints");
					Player.skillpoints += 1;
					Player.defenceHero -= 1;
				}
				Player.skillpoints -= 1;
				defence.setText("Defence: " + (Player.defenceHero + 1));
				availablepoints.setText("" + Player.skillpoints);
				Player.defenceHero += 1;
			}
		}); 

		Label stamina = new Label("Stamina: " + Player.staminaHero);
		grid.add(stamina, 0, 11);

		Button btn3 = new Button();
		btn3.setText(" + ");
		grid.add(btn3, 1, 11);
		btn3.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing stamina+
															// button

			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 0) {
					actiontarget.setText("Out of skillpoints");
					Player.skillpoints += 1;
					Player.staminaHero -= 1;
				}
				Player.skillpoints -= 1;
				stamina.setText("Stamina: " + (Player.staminaHero + 1));
				availablepoints.setText("" + Player.skillpoints);
				Player.staminaHero += 1;
			}
		}); 

		Button btn4 = new Button();
		btn4.setText(" - ");
		grid.add(btn4, 2, 9);
		btn4.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing health-
															// button
			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 10) {
					actiontarget.setText("Skillpoint limit reached");
					Player.skillpoints -= 1;
					Player.healthHero += 1;
				} else if (Player.healthHero == 1) {
					actiontarget.setText("Can't go any lower");
					Player.skillpoints -= 1;
					Player.healthHero += 1;
				}
				Player.skillpoints += 1;
				availablepoints.setText("" + Player.skillpoints);
				Player.healthHero -= 1;
				health.setText("Health: " + (Player.healthHero));
			}
		}); 

		Button btn5 = new Button();
		btn5.setText(" - ");
		grid.add(btn5, 2, 10);
		btn5.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing defence-
															// button
			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 10) {
					actiontarget.setText("Skillpoint limit reached");
					Player.skillpoints -= 1;
					Player.defenceHero += 1;
				} else if (Player.defenceHero == 1) {
					actiontarget.setText("Can't go any lower");
					Player.skillpoints -= 1;
					Player.defenceHero += 1;
				}
				Player.skillpoints += 1;
				availablepoints.setText("" + Player.skillpoints);
				Player.defenceHero -= 1;
				defence.setText("Defence: " + (Player.defenceHero));
			}
		}); 

		Button btn6 = new Button();
		btn6.setText(" - ");
		grid.add(btn6, 2, 11);
		btn6.setOnAction(new EventHandler<ActionEvent>() { // Actionevent for
															// pressing stamina-
															// button
			@Override
			public void handle(ActionEvent e) {
				actiontarget.setText("");
				if (Player.skillpoints == 10) {
					actiontarget.setText("Skillpoint limit reached");
					Player.skillpoints -= 1;
					Player.staminaHero += 1;
				} else if (Player.staminaHero == 1) {
					actiontarget.setText("Can't go any lower");
					Player.skillpoints -= 1;
					Player.staminaHero += 1;
				}
				Player.skillpoints += 1;
				availablepoints.setText("" + Player.skillpoints);
				Player.staminaHero -= 1;
				stamina.setText("Stamina: " + (Player.staminaHero));
			}
		}); 

		Button cont = new Button();
		cont.setText("Continue");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT); 
		hbBtn.getChildren().add(cont);
		grid.add(hbBtn, 3, 15);
		cont.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(Mainmenu.sceneMM());
			}
		});

		Scene sceneCC = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneCC.getStylesheets().addAll(CharacterCreation.class.getResource("/cssFiles/CC.css").toExternalForm());
		return sceneCC;
	}

}
