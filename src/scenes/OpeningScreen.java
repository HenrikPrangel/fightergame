package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import mainGame.MainProgram;

public class OpeningScreen {

	public static StackPane scene1() {

		Button btn1 = new Button();
		btn1.setText("Begin");
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				mainGame.MainProgram.primaryStage.setScene(scene2());
			}
		});

		StackPane root = new StackPane();
		root.getChildren().add(btn1);
		root.getStylesheets().add(MainProgram.class.getResource("/cssFiles/TitleScreen.css").toExternalForm());
		return root;

	}

	public static Scene scene2() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Text introtext1 = new Text(
				"You wake up feeling like someone had shot you in the head. I'ts dark and dank, you can barely see anything.");
		grid.add(introtext1, 0, 0, 3, 1);
		introtext1.setId("intro");

		Text introtext2 = new Text(" Suddenly a monsterous noise comes from the passage ahead");
		grid.add(introtext2, 0, 1, 4, 1);
		introtext2.setId("intro");

		Text introtext3 = new Text(
				" Without the slightest idea of how you got here or what that noise was, the only thing that comes to your mind...  ");
		grid.add(introtext3, 0, 2, 4, 1);
		introtext3.setId("intro");

		Button btn = new Button();
		btn.setText("I must escape this place");
		grid.add(btn, 2, 4);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(Mainmenu.sceneMM());
			}
		});

		Scene sceneIN = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneIN.getStylesheets().addAll(CharacterCreation.class.getResource("/cssFiles/MM.css").toExternalForm());
		return sceneIN;

	}
}
