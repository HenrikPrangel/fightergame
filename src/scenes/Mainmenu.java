package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class Mainmenu {

	public static Scene sceneMM() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		Text scenetitle = new Text("The ESCAPE");
		grid.add(scenetitle, 0, 0, 3, 1);
		scenetitle.setId("Main-Menu");

		Button btn2 = new Button();
		btn2.setText("Begin Fight");
		grid.add(btn2, 1, 4);
		btn2.setOnAction(new EventHandler<ActionEvent>() { // For Beginning
															// fights
			@Override
			public void handle(ActionEvent e) {
				new Level(sceneMM());
			}
		});

		Button btn3 = new Button();
		btn3.setText(" Character Screen ");
		grid.add(btn3, 1, 5);
		btn3.setOnAction(new EventHandler<ActionEvent>() { // To get to the
															// character screen
			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(scenes.CharacterCreation.sceneCC());
			}
		});

		Button btn4 = new Button();
		btn4.setText(" Options ");
		grid.add(btn4, 1, 6);
		btn4.setOnAction(new EventHandler<ActionEvent>() { // For getting to
															// options
			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(sceneOP());
			}
		});

		Button btn5 = new Button();
		btn5.setText(" Quit ");
		grid.add(btn5, 1, 7);
		btn5.setOnAction(new EventHandler<ActionEvent>() { // For quitting

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.close();
			}
		});

		Scene sceneMM = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneMM.getStylesheets().addAll(CharacterCreation.class.getResource("/cssFiles/MM.css").toExternalForm());
		return sceneMM;
	}

	public static Scene sceneOP() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		Button btn = new Button();
		btn.setText(" GAME ");
		grid.add(btn, 0, 1);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(Mainmenu.sceneMM());
			}
		});

		Button btn1 = new Button();
		btn1.setText(" DEVELOPER ");
		grid.add(btn1, 0, 2);
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.Player.skillpoints = 1000;
				mainGame.Player.healthHero = 1000;
				mainGame.Player.defenceHero = 1000;
				mainGame.Player.staminaHero = 1000;
			}
		});

		Scene sceneOP = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneOP.getStylesheets().addAll(Level.class.getResource("/cssFiles/FM.css").toExternalForm());
		return sceneOP;
	}

}
