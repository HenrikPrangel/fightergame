package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class Victory {
	public static Scene sceneV() { // Scene for winning a level

		mainGame.Player.skillpoints += 5;

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Label gg = new Label("Victory");
		grid.add(gg, 0, 1);
		gg.setId("well-done");

		Label level = new Label("You got 5 extra skillpoints");
		grid.add(level, 0, 2);

		Label clevel = new Label("Current level:" + scenes.Level.currentLevel);
		grid.add(clevel, 0, 3);

		Button btn = new Button();
		btn.setText(" Continue ");
		grid.add(btn, 0, 7);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				mainGame.MainProgram.primaryStage.setScene(scenes.Mainmenu.sceneMM());

			}
		});

		final Text actiontarget = new Text();
		grid.add(actiontarget, 1, 5);
		actiontarget.setId("actiontarget");

		if (Level.currentLevel > 10) {
			Button btn1 = new Button();
			btn1.setText(" You see something glimmer... ");
			grid.add(btn1, 0, 8);
			btn1.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent e) {
					mainGame.MainProgram.primaryStage.setScene(sceneE());
				}
			});
		}

		if (randomNbr() > 60) {
			mainGame.Player.potions += 1;
			actiontarget.setText("Exhausted from battle, you rejoice as you see a potion lying behind a rock");
		}

		Scene sceneL = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneL.getStylesheets().addAll(Level.class.getResource("/cssFiles/VM.css").toExternalForm());
		return sceneL;

	}

	public static Scene sceneE() { // Scene for winning entire game
		scenes.Level.b = monster.Monster.randomMonster();
		scenes.Level.a = monster.Monster.monsterWeakness(scenes.Level.b);
		scenes.Level.mname = monster.Monster.monsterName(scenes.Level.b);
		mainGame.Player.skillpoints += 5;

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Label youWon = new Label("As you walk towards the glimmering light you realise...");
		grid.add(youWon, 0, 1);

		Label itsOver = new Label("It's an opening to the surface!");
		itsOver.setId("well-done");
		grid.add(itsOver, 0, 2);

		Button btn = new Button();
		btn.setText(" ESCAPE ");
		grid.add(btn, 3, 7);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.close();
			}
		});

		Scene sceneV = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneV.getStylesheets().addAll(Level.class.getResource("/cssFiles/Escape.css").toExternalForm());
		return sceneV;
	}

	public static int randomNbr() { // method, that makes a random number
		int vahemik = 100 - 1 + 1;
		return 1 + (int) (Math.random() * vahemik);
	}

}
