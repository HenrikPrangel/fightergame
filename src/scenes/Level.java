package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class Level extends monster.Monster {

	public static int t = 2; // Amount of actions per turn
	public static int currentLevel = 0;
	public static int skippedlevels = 0;
	public static int isdefending = 0;

	public Level(Scene scene2) {
		mainGame.MainProgram.primaryStage.setScene(sceneFM());
	}

	public static Scene sceneFM() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Label remainingHealth = new Label("HEALTH:" + mainGame.Player.healthHero);
		grid.add(remainingHealth, 6, 10);

		Label currentStamina = new Label("STAMINA:" + mainGame.Player.staminaHero);
		grid.add(currentStamina, 6, 11);

		Text monsterName = new Text(mslur + ": ");
		monsterName.setId("name1");
		grid.add(monsterName, 0, 1);

		Text monsterName2 = new Text(mname);
		monsterName2.setId("name2");
		grid.add(monsterName2, 1, 1);

		Image image = new Image(mpicture);
		ImageView iv1 = new ImageView();
		iv1.setImage(image);
		iv1.setImage(image);
		iv1.setFitWidth(300);
		iv1.setPreserveRatio(true);
		iv1.setSmooth(true);
		iv1.setCache(true);
		grid.add(iv1, 0, 8);

		Label mmonsterHealth = new Label("Health: " + monsterHealth);
		grid.add(mmonsterHealth, 0, 2);

		final Text actiontarget = new Text();
		grid.add(actiontarget, 6, 12);
		actiontarget.setId("actiontarget");

		Button btn1 = new Button(); // First attack button that splits into 3
									// other attack buttons on press
		btn1.setText(" Attack ");
		grid.add(btn1, 1, 14);
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				Button btn4 = new Button();
				btn4.setText(" Head ");
				grid.add(btn4, 1, 12);
				btn4.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent e) {
						if (t == 0 || t < 0) {
							actiontarget.setText("Out of skillpoints");
						} else if (a == 0) {
							monster.Monster.attackWeakMonster();
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						} else {
							monster.Monster.attackMonster(a);
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						}
						t = t - 1;
					}
				});

				Button btn5 = new Button();
				btn5.setText(" Upper body ");
				grid.add(btn5, 2, 12);
				btn5.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent e) {
						if (t == 0 || t < 0) {
							actiontarget.setText("Out of skillpoints");
						} else if (a == 1) {
							monster.Monster.attackWeakMonster();
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						} else {
							monster.Monster.attackMonster(a);
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						}
						t = t - 1;
					}
				});

				Button btn6 = new Button();
				btn6.setText(" Lower body ");
				grid.add(btn6, 3, 12);
				btn6.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent e) {
						if (t == 0 || t < 0) {
							actiontarget.setText("Out of skillpoints");
						} else if (a == 2) {
							monster.Monster.attackWeakMonster();
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						} else {
							monster.Monster.attackMonster(a);
							mainGame.MainProgram.primaryStage.setScene(sceneFM());
						}
						t = t - 1;
					}
				});
			}
		});

		Button btn2 = new Button();
		btn2.setText(" Defend ");
		grid.add(btn2, 2, 14);
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				if (t == 0 || t < 0) {
					actiontarget.setText("Out of skillpoints");
				} else {
					isdefending += 1;
					mainGame.MainProgram.primaryStage.setScene(sceneFM());
					t = t - 1;
				}
			}
		});

		Button btn3 = new Button();
		btn3.setText(" Escape ");
		grid.add(btn3, 5, 14);
		btn3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				if (Victory.randomNbr() < 50) {
					skippedlevels += 1;
					endLevel();
					mainGame.MainProgram.primaryStage.setScene(Mainmenu.sceneMM());
				} else if (mainGame.Player.healthHero == 0 || mainGame.Player.healthHero < 0) {
					mainGame.MainProgram.primaryStage.setScene(Defeat.sceneD());
				} else
					endTurn();
			}
		});

		Button btn7 = new Button();
		btn7.setText(" Drink Potion ");
		grid.add(btn7, 3, 14);
		btn7.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				if (t == 0 || t < 0) {
					actiontarget.setText("Out of skillpoints");
				} else if (mainGame.Player.potions > 0) {
					mainGame.Player.potions -= 1;
					mainGame.Player.healthHero = mainGame.Player.healthHero + 6;
					mainGame.MainProgram.primaryStage.setScene(sceneFM());
					t = t - 1;
				} else
					actiontarget.setText("Out of Potions");
			}
		});

		Button btn8 = new Button();
		btn8.setText(" End Turn ");
		grid.add(btn8, 4, 14);
		btn8.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				if (monsterHealth == 0 || monsterHealth < 0) {
					endLevel();
					mainGame.MainProgram.primaryStage.setScene(Victory.sceneV());
				} else if (mainGame.Player.healthHero == 0 || mainGame.Player.healthHero < 0) {
					mainGame.MainProgram.primaryStage.setScene(Defeat.sceneD());
				} else
					endTurn();
			}
		});

		Scene sceneFM = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneFM.getStylesheets().addAll(Level.class.getResource("/cssFiles/FM.css").toExternalForm());
		return sceneFM;
	} // End of scene

	public static void endTurn() { // Method for ending the turn during fight

		t = 2;
		if (isdefending == 2) {
			mainGame.Player.healthHero = (int) (mainGame.Player.healthHero
					- ((1 + 1.6 * currentLevel) / (mainGame.Player.defenceHero / 3)));
			monsterHealth -= (2 + (currentLevel / 10));
		} else if (isdefending == 1) {
			mainGame.Player.healthHero = (int) (mainGame.Player.healthHero
					- ((2 + 1.6 * currentLevel) / (mainGame.Player.defenceHero / 5)));
		} else {
			mainGame.Player.healthHero = (int) (mainGame.Player.healthHero - (2 + 1.6 * currentLevel));
		}
		isdefending = 0;
		mainGame.MainProgram.primaryStage.setScene(sceneFM());
	}

	public static void endLevel() { // Method that ends the level, thereby
									// resetting the monstername and weakness
		b = randomMonster();
		a = monsterWeakness(b);
		mname = monsterName(b);
		mpicture = monsterPicture(b);
		mslur = monsterSlur(randomMonster());
		mainGame.Player.healthHero += 5;
		monsterHealth = 20;
		currentLevel += 1;
		t = 2;
	}

}
