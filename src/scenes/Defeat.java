package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class Defeat {

	public static Scene sceneD() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Label gg = new Label("You have been defeated");
		gg.setId("gg");
		grid.add(gg, 0, 1);

		Label level = new Label("You got to level " + Level.currentLevel);
		grid.add(level, 0, 2);

		Label skipped = new Label("You skipped " + Level.skippedlevels + " levels");
		grid.add(skipped, 0, 3);

		Button btn1 = new Button();
		btn1.setText(" Quit ");
		grid.add(btn1, 1, 7);
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.close();
			}
		});

		Scene sceneD = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		sceneD.getStylesheets().addAll(Level.class.getResource("/cssFiles/FM.css").toExternalForm());
		return sceneD;

	}
}
