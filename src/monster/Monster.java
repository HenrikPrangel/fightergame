package monster;

import mainGame.MainProgram;
import mainGame.Player;

public class Monster {

	public static int monsterHealth = 20;
	public static String monsterNames[] = { "Juhhanus", "Milvi", "Young boy", "Murrloc", "Randomus", "Pepegator",
			"Krankenwagen", "Nicolagus Cagus", "GromsY", "Harambumus", "Kando Rurel", "Juan Cenus", "Neo",
			"Ared Flliksaar" };

	public static String monsterSlurs[] = { "The hideous", "The ugly", "The abominable", "The Clumsy", "A Naughty",
			"A really tired", "The two days from retirement ", "The unbelievable", "The loudmouthed",
			"His royal highness", "A racist", "A Dank", "A misunderstood ", "A WOW nerd called" };

	public static String monsterPictures[] = { "monster/1.jpg", "monster/2.jpg", "monster/3.jpg", "monster/4.jpg",
			"monster/5.jpg", "monster/6.jpg", "monster/7.jpg", "monster/8.jpg", "monster/9.jpg", "monster/10.jpg",
			"monster/11.jpg", "monster/12.jpg", "monster/13.jpg", "monster/14.jpg" };

	public static int monsterWeakness[][] = MainProgram.createRandomMatrix(monsterNames);

	public static int b = randomMonster(); // Chooses a random
	// monster, and
	// returns its
	// position in the
	// array

	public static String mname = monsterName(b); // Returns the
	// name of
	// the
	// current
	// monster

	public static String mslur = monsterSlur(randomMonster()); // Returns a
	// random slur

	public static String mpicture = monsterPicture(b);
	public static int a = monsterWeakness(b); // Finds the
	// weakspot of
	// the monster
	// for the
	// current fight

	public static int monsterWeakness(int namePosition) { // Method that reads
															// either 1, 2 or 3
															// from the matrix,
															// thereby giving a
															// weakspot to the
															// monster
		int a = monsterWeakness[namePosition][1];
		return a;
	}

	public static void attackMonster(int a) { // Mehtod if attacking normal spot
		monsterHealth -= (Player.staminaHero / 10);
	}

	public static void attackWeakMonster() { // Method if attacking weakspot
		monsterHealth -= (Player.staminaHero / 10) + (1 * (Player.staminaHero / 10));
	}

	public static int randomMonster() { // Chooses a random monster

		int namePosition = (int) (Math.random() * monsterNames.length);

		return namePosition;
	}

	public static String monsterName(int namePosition) { // Method that returns
															// the name of the
															// current monster
		String name = "";
		int nameC = namePosition;
		for (int i = 0; i < monsterNames.length; i++) {
			if (nameC == i) {
				name = monsterNames[i];
			}
		}
		return name;
	}

	public static String monsterSlur(int namePosition) { // Chooses a random
															// slur
		String slur = "";
		int nameC = namePosition;
		for (int i = 0; i < monsterSlurs.length; i++) {
			if (nameC == i) {
				slur = monsterSlurs[i];
			}
		}
		return slur;
	}

	public static String monsterPicture(int namePosition) { // Returns picture
															// of current
															// monster
		String pic = "";
		int nameC = namePosition;
		for (int i = 0; i < monsterPictures.length; i++) {
			if (nameC == i) {
				pic = monsterPictures[i];
			}
		}
		return pic;
	}
}
